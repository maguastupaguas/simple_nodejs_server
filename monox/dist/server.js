"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Server = exports.Server = function Server(name, serverName, port, isDefault, root, accessLog, locations, errorPages) {
    _classCallCheck(this, Server);

    this._name = name;
    this._serverName = serverName;
    this._port = port;
    this._isDefault = isDefault;
    this._root = root;
    this._accessLog = accessLog;
    this._locations = locations;
    this._errorPages = errorPages;
};