'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.App = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _path = require('path');

var _serverInfo = require('./serverInfo.js');

var _connectionManager = require('./connectionManager.js');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var App = exports.App = function () {
    function App(config) {
        _classCallCheck(this, App);

        this._servers = configServers(config);
        this._connections = new _connectionManager.Servers(this.ports(), this._servers);
    }

    _createClass(App, [{
        key: 'ports',
        value: function ports() {
            return this._servers.map(function (s) {
                return s._port;
            }).filter(function (current, index, array) {
                return array.indexOf(current) === index;
            });
        }
    }]);

    return App;
}();

function configServers(config) {
    return Object.keys(config.server).map(function recorrer(name) {
        var port = void 0;
        var isDefault = void 0;
        var serverConfig = config.server[name];
        var listenRequest = serverConfig.listen;
        var serverName = serverConfig.serverName;

        if (Number.isInteger(listenRequest)) {
            port = listenRequest;
            isDefault = false;
        } else {
            var split = listenRequest.split(" ");
            port = Number.parseInt(split[0]);
            isDefault = split[1] === "default_server";
        }

        var root = (0, _path.resolve)(serverConfig.root);
        var accessLog = (0, _path.resolve)(serverConfig.accessLog);
        // locations
        // errorPages

        var server = new _serverInfo.ServerInfo(name, serverName, port, isDefault, root, accessLog, {}, {});

        return server;
    });
}