'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Response = exports.Request = exports.parseRequestResponse = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var parseRequestResponse = exports.parseRequestResponse = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee(socket) {
        var plainRequestPromise, plainRequest, _plainRequest$split, _plainRequest$split2, head, splitBody, body, _head$split, _head$split2, firstLine, rawHeaders, _firstLine$split, _firstLine$split2, method, path, version, headers, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, header, _header$split, _header$split2, name, value, url;

        return _regenerator2.default.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        plainRequestPromise = new Promise(function (resolve, reject) {
                            socket.on('data', function (req) {
                                resolve(req.toString());
                            });
                        });
                        _context.next = 3;
                        return plainRequestPromise;

                    case 3:
                        plainRequest = _context.sent;
                        _plainRequest$split = plainRequest.split('\r\n\r\n'), _plainRequest$split2 = _toArray(_plainRequest$split), head = _plainRequest$split2[0], splitBody = _plainRequest$split2.slice(1);
                        body = splitBody.join('\r\n\r\n');
                        _head$split = head.split('\r\n'), _head$split2 = _toArray(_head$split), firstLine = _head$split2[0], rawHeaders = _head$split2.slice(1);
                        _firstLine$split = firstLine.split(' '), _firstLine$split2 = _slicedToArray(_firstLine$split, 3), method = _firstLine$split2[0], path = _firstLine$split2[1], version = _firstLine$split2[2];
                        headers = new Map();
                        _iteratorNormalCompletion = true;
                        _didIteratorError = false;
                        _iteratorError = undefined;
                        _context.prev = 12;

                        for (_iterator = rawHeaders[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                            header = _step.value;
                            _header$split = header.split(':'), _header$split2 = _slicedToArray(_header$split, 2), name = _header$split2[0], value = _header$split2[1];

                            headers.set(name.trim().toLowerCase(), value.trim());
                        }

                        _context.next = 20;
                        break;

                    case 16:
                        _context.prev = 16;
                        _context.t0 = _context['catch'](12);
                        _didIteratorError = true;
                        _iteratorError = _context.t0;

                    case 20:
                        _context.prev = 20;
                        _context.prev = 21;

                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }

                    case 23:
                        _context.prev = 23;

                        if (!_didIteratorError) {
                            _context.next = 26;
                            break;
                        }

                        throw _iteratorError;

                    case 26:
                        return _context.finish(23);

                    case 27:
                        return _context.finish(20);

                    case 28:
                        url = urlModule.parse('http://' + headers.get('host') + path);


                        if (!url.port) {
                            url.port = socket.localPort.toString();
                        }

                        // console.log(url)

                        return _context.abrupt('return', [new Request(socket, url, headers, body, version, method), new Response(socket, version)]);

                    case 31:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[12, 16, 20, 28], [21,, 23, 27]]);
    }));

    return function parseRequestResponse(_x) {
        return _ref.apply(this, arguments);
    };
}();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _toArray(arr) { return Array.isArray(arr) ? arr : Array.from(arr); }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

// import { parse, UrlObject } from 'url';
var urlModule = require('url');

var Request = exports.Request = function () {
    function Request(socket, url, headers, body, version, method) {
        _classCallCheck(this, Request);

        this._socket = socket;
        this._url = url;
        this._headers = headers;
        this._body = body;
        this._httpVersion = version;
        this._method = method;
    }

    _createClass(Request, [{
        key: 'ipAddress',
        value: function ipAddress() {
            return this._socket.remoteAddress;
        }
    }]);

    return Request;
}();

var Response = exports.Response = function Response(socket, version) {
    _classCallCheck(this, Response);

    this._headers = {};
    this._statusCode = {};
    this._statusMessage = "";
    this._socket = socket;
    this._httpVersion = version;
};