'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Servers = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _requestResponse = require('./requestResponse.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var net = require('net');
var fs = require('fs');

var Servers = exports.Servers = function () {
    function Servers(ports, servers) {
        _classCallCheck(this, Servers);

        this._serverInstances = new Map();
        // callback?
        this._ports = ports;
        this._servers = servers;
        this.begin();
    }

    _createClass(Servers, [{
        key: 'begin',
        value: function begin() {
            var servers = this._servers;

            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = this._ports[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var port = _step.value;

                    var serverI = net.createServer({ allowHalfOpen: true }, function () {
                        var _ref = _asyncToGenerator( /*#__PURE__*/_regenerator2.default.mark(function _callee(socket) {
                            var _ref2, _ref3, req, res;

                            return _regenerator2.default.wrap(function _callee$(_context) {
                                while (1) {
                                    switch (_context.prev = _context.next) {
                                        case 0:
                                            _context.next = 2;
                                            return (0, _requestResponse.parseRequestResponse)(socket);

                                        case 2:
                                            _ref2 = _context.sent;
                                            _ref3 = _slicedToArray(_ref2, 2);
                                            req = _ref3[0];
                                            res = _ref3[1];

                                            requestManager(req, res, servers);

                                        case 7:
                                        case 'end':
                                            return _context.stop();
                                    }
                                }
                            }, _callee, this);
                        }));

                        function requestHandler(_x) {
                            return _ref.apply(this, arguments);
                        }

                        return requestHandler;
                    }());

                    serverI.on('error', function (err) {
                        throw err;
                    });

                    serverI.listen(port, function () {
                        console.log("server bound");
                    });

                    this._serverInstances.set(port, serverI);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }
    }]);

    return Servers;
}();

function requestManager(req, res, servers) {
    var server = servers.find(function portNHostName(s) {
        if (s._port === Number.parseInt(req._url.port) && s._serverName === req._url.hostname) return s;
    });

    if (server === 'undefined') {
        server = servers.find(function defaultS(s) {
            if (s._isDefault && s._port === req._url.port) return s;
        });
    }
    sendResponse(req, res, server);
}

function sendResponse(req, res, server) {
    var absPath = server._root + req._url.path;

    if (fs.existsSync(absPath)) {
        putHeaders(req, res, server, absPath);

        var readStream = fs.createReadStream(absPath);
        readStream.pipe(res._socket);

        // readStream.pipe(process.stdout); // ok
    } else {
        if (res._socket.write("Error 404")) console.log("fue true");
    }
}

function putHeaders(req, res, server, absPath) {
    res._headers.date = "Date: " + new Date().toString();
    res._headers.contentType = "Content-Type: " + absPath.split(".").pop();
    res._headers.contentLength = "Content-Length: " + fs.statSync(absPath).size;
    res._headers.server = "Server: " + server._serverName;

    res._socket.write(res._httpVersion + ' 200 OK\n');
    res._socket.write(res._headers.toString());
    console.log(res._headers);
}