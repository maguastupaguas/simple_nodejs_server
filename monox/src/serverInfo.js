

export class ServerInfo{
    constructor(name, serverName, port, isDefault, root, accessLog, locations, errorPages){
        this._name = name;
        this._serverName = serverName;
        this._port = port;
        this._isDefault = isDefault;
        this._root = root;
        this._accessLog = accessLog;
        this._locations = locations;
        this._errorPages = errorPages;
    }


}