import {resolve} from 'path';

import {ServerInfo} from './serverInfo.js';
import {Servers} from './connectionManager.js';


export class App {
    constructor(config){
        this._servers = configServers(config);
        this._connections = new Servers(this.ports(), this._servers);

    }

    ports() {
        return this._servers
            .map(s => s._port)
            .filter((current, index, array) => array.indexOf(current) === index);
    }
    
}


function configServers(config){
    return Object.keys(config.server).map(function recorrer(name){
        let port;
        let isDefault;
        let serverConfig = config.server[name];
        let listenRequest = serverConfig.listen;
        let serverName = serverConfig.serverName;

        if (Number.isInteger(listenRequest)){
            port = listenRequest;
            isDefault = false;
        } else{
            const split = listenRequest.split(" ");
            port = Number.parseInt(split[0]);
            isDefault = (split[1] === "default_server");
        }
    
        const root = resolve(serverConfig.root);
        const accessLog = resolve(serverConfig.accessLog); 
        // locations
        // errorPages
    
        const server = new ServerInfo(name,
            serverName,
            port, 
            isDefault,
            root,
            accessLog, 
            {}, 
            {}
        );
    
        return server;
    })

}