// import { parse, UrlObject } from 'url';
const urlModule = require('url');

export async function parseRequestResponse(socket) {
    const plainRequestPromise = new Promise((resolve, reject) => {
        socket.on('data', (req) => {
            resolve(req.toString());
        });
    });
    const plainRequest = await plainRequestPromise;

    const [head, ...splitBody] = plainRequest.split('\r\n\r\n');
    const body = splitBody.join('\r\n\r\n');
    const [firstLine, ...rawHeaders] = head.split('\r\n');
    const [method, path, version] = firstLine.split(' ');
    const headers = new Map();
    for (const header of rawHeaders) {
        const [name, value] = header.split(':');
        headers.set(name.trim().toLowerCase(), value.trim());
    }

    const url = urlModule.parse(`http://${headers.get('host')}${path}`);

    if (!url.port) {
        url.port = socket.localPort.toString();
    }

    // console.log(url)

    return [
        new Request(socket, url, headers, body, version, method),
        new Response(socket, version)
    ];
}

export class Request{
    constructor(socket, url, headers, body, version, method){
        this._socket = socket;
        this._url = url;
        this._headers = headers;
        this._body = body;
        this._httpVersion = version;
        this._method = method;
    }

    ipAddress(){
        return this._socket.remoteAddress;
    }
}

export class Response{
    constructor(socket, version){
        this._headers = {};
        this._statusCode = {};
        this._statusMessage = "";
        this._socket = socket;
        this._httpVersion = version;
    }

}