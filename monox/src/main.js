import {resolve} from 'path';
import { readJsonAsync } from './utils.js';
import {App} from './configServers.js';


async function main(){
    const pathConfig = resolve('config.json');
    const config = await readJsonAsync(pathConfig);
    const app = new App(config);
}

main();

