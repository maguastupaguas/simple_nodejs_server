const net = require('net');
const fs = require('fs');

import {parseRequestResponse} from './requestResponse.js';

export class Servers{

    constructor (ports, servers){
        this._serverInstances = new Map();
        // callback?
        this._ports = ports;
        this._servers = servers;
        this.begin();
    }

    begin(){
        let servers =  this._servers;

        for (const port of this._ports){
            const serverI = net.createServer({allowHalfOpen: true}, 
                async function requestHandler(socket){
                    const [req, res] = await parseRequestResponse(socket);
                    requestManager(req, res, servers);
                }
            );

            serverI.on('error', (err) => {
                throw err;
            });

            serverI.listen(port, () => {
                console.log("server bound")
            });

            this._serverInstances.set(port, serverI);
        }
    }
}

function requestManager(req, res, servers){
    let server = servers.find(function portNHostName(s){
        if (s._port === Number.parseInt(req._url.port) && s._serverName === req._url.hostname)
            return s;
    });

    if (server === 'undefined'){
        server = servers.find(function defaultS(s) {
            if (s._isDefault && s._port === req._url.port)
                return s;
        });
    }
    sendResponse(req, res, server);
}

function sendResponse(req, res, server){
    let absPath = server._root + req._url.path;

    if (fs.existsSync(absPath)){
        putHeaders(req, res, server, absPath)

        let readStream = fs.createReadStream(absPath);
        readStream.pipe(res._socket);

        // readStream.pipe(process.stdout); // ok
    } else{
        if (res._socket.write("Error 404"))
            console.log("fue true")
    }
}

function putHeaders(req, res, server, absPath){
    res._headers.date = "Date: " + (new Date()).toString();
    res._headers.contentType = "Content-Type: " + absPath.split(".").pop();
    res._headers.contentLength = "Content-Length: " + fs.statSync(absPath).size;
    res._headers.server = "Server: " + server._serverName;
    
    res._socket.write(`${res._httpVersion} 200 OK\n`)
    res._socket.write(res._headers.toString())
    console.log(res._headers)
}