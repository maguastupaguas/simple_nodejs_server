import { readFile } from 'fs';

export async function readFileAsync(name, options) {
    return new Promise((resolve, reject) => {
        readFile(name, options, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}

export async function readJsonAsync(name, options) {
    const file = await readFileAsync(name, options);
    return JSON.parse(file.toString());
}

export * from 'fs';
